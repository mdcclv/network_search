#require 'capybara/poltergeist'

#Capybara.javascript_driver = :poltergeistb

require "rubygems"
require "bundler/setup"
require "capybara"
require "site_prism"
require "scraper_controller"


class DoctorSearchController < ApplicationController
  include Capybara::DSL

  Capybara.run_server = false
  Capybara.app_host = "https://www.blueshieldca.com/"
  Capybara.current_driver = :selenium

  def index
  end

  def results
    if params[:last]
      #@networks = { blueshield: true, cigna: false }
      @networks = { 
        blueshield: bs_results(params[:first],params[:last]),
        cigna:      ci_results(params[:first],params[:last])
      }
    else
      render "index"
    end
  end

  def bs_results(first,last)
    page = BlueShieldSearch.new
    page.load
    page.activate_search.click
    page.first_name.set(first)
    page.last_name.set(last)
    page.submit.click
    page.wait_for_continue_button
    page.continue_button.click
    page = BlueShieldResult.new
    page.wait_for_result_count(5)
    page.has_doc_error? and page.doc_error.visible? and return false
    page.has_result_count? and page.result_count.visible? and return true
  end

  def ci_results(first,last)
    page = CignaSearch.new
    page.load
    page.activate_search.select("Person By Name")
    page.search_location.set("CA")
    page.name_text.set("#{first} #{last}")
    page.submit.click
    page = CignaResult.new
    page.has_result_count? and return true
    page.has_doc_error? and return false
  end

end


