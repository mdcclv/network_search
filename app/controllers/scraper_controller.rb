class BlueShieldScraperController < ApplicationController
  def search_for(fname,lname)
    # post to https://www.blueshieldca.com/fap/app/searchResults.html
    # formData: 
  end

end

class BlueShieldSearch < SitePrism::Page
  set_url "https://www.blueshieldca.com/fap/app/search.html"

  element :activate_search, "a[id='doctorsAdvancedSearch']"
  element :doctor_select, "input[id='provider_Doctors']"
  element :first_name, "input[id='doctorFName']"
  element :last_name, "input[id='doctorLName']"
  element :submit, "li[id='findnow'] input.findNow"

  element :continue_button, "div#lightBoxContent input.continueBtn"
end

class BlueShieldResult < SitePrism::Page
  set_url "https://www.blueshieldca.com/fap/app/search.html"
  element :doc_error, "span[id='docErrorId']"
  element :result_count, "h3#tab1 span.resultCount" # present == results
end

class CignaSearch < SitePrism::Page
  set_url "http://www.cigna.com/web/public/hcpdirectory"
  element :activate_search, "select#landingPageSelectedDropDownValue"
  element :name_text, "input#lookingForText"
  element :search_location, "input#searchLocation"
  element :submit, "button#searchLocBtn"
end

class CignaResult < SitePrism::Page
  set_url "http://www.cigna.com/web/public/hcpdirectory/searchresults/"
  element :result_count, "label#count-doctor"
  element :doc_error, "div#noResultsErrorMessage"
end
